#Server
import socket			

s = socket.socket()		
print ("Socket successfully created")
port = 10805

s.bind(('', port))		
print ("socket binded to %s" %(port))

s.listen(5)	
print ("socket is listening")		

n = 1
c, addr = s.accept()	
print ('Got connection from', addr )
c.send(str(n).encode())

while True:
  data = c.recv(1024).decode()
  
  if data:
    print (data)
    n = int(data)+1
    if(n-1 == 100): 
      c.close()
      break
    else:
      c.send(str(n).encode())

# ให้เขียนโปรแกรมส่งตัวเลขจาก
# server ไปยัง client โดยค่าเริ่มต้นของตัวเลขคือ 1
# หลังจาก client ได้รับค่าจาก server แล้วให้ print ออกหน้าจอ
# และส่งค่าที่ได้รับมา +1 ให้กับ server
# server print ค่าออกหน้าจอ
# ส่งกลับไปกลับมาแบบนี้จนค่าของตัวเลขเท่ากับ 100

#   1
#   s     c    monitor
#   ----->1----->1
#         1+1
# 2 <-----2
# print 2
# 2+1----->3----->3



# # Server:
# # first of all import the socket library
# import socket            
 
# # next create a socket object
# s = socket.socket()        
# print ("Socket successfully created")
 
# # reserve a port on your computer in our
# # case it is 12345 but it can be anything
# port = 12345              
 
# # Next bind to the port
# # we have not typed any ip in the ip field
# # instead we have inputted an empty string
# # this makes the server listen to requests
# # coming from other computers on the network
# s.bind(('', port))        
# print ("socket binded to %s" %(port))
 
# # put the socket into listening mode
# s.listen(5)    
# print ("socket is listening")          
 
# # a forever loop until we interrupt it or
# # an error occurs
# while True:
 
# # Establish connection with client.
#     c, addr = s.accept()    
#     print ('Got connection from', addr )
 
#   # send a thank you message to the client. encoding to send byte type.
#     c.send('Thank you for connecting'.encode())
 
#   # Close the connection with the client
#     c.close()
   
#   # Breaking once connection closed
#     break