#Client
import socket			

s = socket.socket()		

port = 10805

s.connect(('127.0.0.1', port))

while True:
  st = s.recv(1024).decode()
  if st:
    print (st)
    n = int(st)+1
    # print(n)
    strn = str(n)
    s.send(strn.encode())
    if(n == 100):
      s.close()
      break
	

# ให้เขียนโปรแกรมส่งตัวเลขจาก
# server ไปยัง client โดยค่าเริ่มต้นของตัวเลขคือ 1
# หลังจาก client ได้รับค่าจาก server แล้วให้ print ออกหน้าจอ
# และส่งค่าที่ได้รับมา +1 ให้กับ server
# server print ค่าออกหน้าจอ
# ส่งกลับไปกลับมาแบบนี้จนค่าของตัวเลขเท่ากับ 100

#   1
#   s     c    monitor
#   ----->1----->1
#         1+1
# 2 <-----2
# print 2
# 2+1----->3----->3


# # Client:
# # Import socket module
# import socket            
 
# # Create a socket object
# s = socket.socket()        
 
# # Define the port on which you want to connect
# port = 12345               
 
# # connect to the server on local computer
# s.connect(('127.0.0.1', port))
 
# # receive data from the server and decoding to get the string.
# print (s.recv(1024).decode())
# # close the connection
# s.close()